---
title: "Learn theme"
date: 2021-02-13T12:45:35+01:00
draft: false
---

[GitHub repo of Learn](https://github.com/matcornic/hugo-theme-learn), MIT license.

1. Create a new chapter with:
```bash
hugo new --kind chapter hugo/_index.md
```

2. Create a new entry.
```
hugo new hugo/quick_start.md
```